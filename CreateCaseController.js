({
    doInit : function(component, event, helper) {
        var ca = document.cookie;
		console.log('ca-----');
        component.set("v.runningUserId", $A.get("$SObjectType.CurrentUser.Id"));
        console.log('controller getMetadata');
        helper.getMetadata(component, event, helper)
        .then(function() {
            console.log('controller getUserInfo');
            return helper.getUserInfo(component, event, helper);
        })
        .then(function() {
            console.log('controller getContactInfo');
            return helper.getContactInfo(component, event, helper);
        })
        .then(function() {
            console.log('controller getIPAddress');
            return helper.getIPAddress(component, event, helper);
        })
        .then(function() {
            console.log('controller getBrowserInformation');
            return helper.getBrowserInformation(component, event, helper);
        })
        .then(function() {
            console.log('controller setCaptchaListener');
            return helper.setCaptchaListener(component, event, helper);

        })
        .then(function() {
            console.log('controller getOperatingSystemPicklist');
            return helper.getOperatingSystemPicklist(component, event, helper);
        })
        .then(function() {
            console.log('controller getSuspensionReasonPicklist');
            console.log('spinner off');
            let spinner = component.find("spinner");
            $A.util.addClass(spinner, 'slds-hide');
            return helper.getSuspensionReasonPicklist(component, event, helper);
        })
        .catch(function(error) {
            console.log('controller catch');
            helper.showError(component, event, helper, error);
            let spinner = component.find("spinner");
            $A.util.addClass(spinner, 'slds-hide');
        });

    },

    itemsChange : function(component, event, helper) {
        helper.fireItemsChangeEvent(component, event, helper);
    },

    loginToPassport : function(component, event, helper) {
        helper.loginToPassport(component, event, helper);
    },

    redirectToPassport : function(component, event, helper) {
        helper.redirectToPassport(component, event, helper);
    },

    validateRequired : function(component, event, helper) {
        helper.validateRequired(component, event, helper);
    },

    categoryChange : function(component, event, helper) {
        helper.categoryChange(component, event, helper);
    },

    subCategoryChange : function(component, event, helper) {
        helper.subCategoryChange(component, event, helper);
    },

    detailCategoryChange : function(component, event, helper) {
        helper.detailCategoryChange(component, event, helper);
    },

    toggleReRender : function(component, event, helper) {
        helper.toggleReRender(component, event, helper);
    },

    toggleOptions : function(component, event, helper) {
        helper.toggleOptions(component, event, helper);
    },

    partnerAffiliateToggle : function(component, event, helper) {
        helper.partnerAffiliateToggle(component, event, helper);
    },

    setPartner : function(component, event, helper) {
        component.set("v.isPartner", !component.get("v.isPartner"));
        helper.partnerAffiliateToggle(component, event, helper);
    },

    onRecordSubmit : function(component, event, helper) {
        console.log('rec submit controller');
        event.preventDefault();
        var spinner = component.find("spinner");
        $A.util.toggleClass(spinner, "slds-hide");
        helper.onRecordSubmit(component, event, helper);
    },

    onRecordSuccess : function(component, event, helper) {
        helper.onRecordSuccess(component, event, helper);
    },

    handleUploadFinished: function (component, event, helper) {
        helper.handleUploadFinished(component, event, helper);
    },

    handleRemove: function (component, event) {
        var ctarget = event.currentTarget;
        var fileName = ctarget.dataset.value;
        console.log(fileName);
        var fileList = component.get("v.fileList");
        for(var i = 0; i < fileList.length; i++) {
            if(fileList[i].name == fileName) {
                fileList.splice(i,1);
                break;
            }
        }

        component.set("v.fileList", fileList);
    },

})