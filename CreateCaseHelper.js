({
    getMetadata : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getMetadata');
            action.setCallback(this, function(response) {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                }
                var state = response.getState();
                if(state === "SUCCESS") {
                    var response = response.getReturnValue();
                    var wrapper = JSON.parse(response);
                    var cats = wrapper.categories;
                    var catStrToSubCats = wrapper.catStrToSubCats
                    var subCatStrToDetCats = wrapper.subCatStrToDetCats;

                    component.set("v.webCaseWrapper", wrapper);
                    component.set("v.categories", cats);

                    resolve();
                }
                else {
                    reject($A.get('$Label.c.cs_get_metadata_error'));
                }
            });

            $A.enqueueAction(action);
        }));
    },

    getUserInfo : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var runningUserId = component.get("v.runningUserId");
            if(runningUserId) {
                var action = component.get('c.getUserInfo');
                action.setParams({
                    userId : runningUserId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        var response = response.getReturnValue();
                        var user = JSON.parse(response)[0];
                        component.set("v.name", user.Name);
                        component.set("v.email", user.Email);
                        component.set("v.twitchId", user.TwitchUserID__c);
                        component.set("v.twitchUserName", user.TwitchUserName__c);
                        component.set("v.twitchLoginAuthorized", true);
                        component.set("v.loginAuthorized", true);
                        component.set("v.userContactId", user.ContactId);

                        resolve();
                    }
                    else {
                        reject($A.get('$Label.c.cs_get_user_info_error'));
                    }
                });

                $A.enqueueAction(action);
            }
            else {
                resolve();
            }
        }));
    },

    getContactInfo : function(component, event, helper) {
        var self = this;
        return new Promise($A.getCallback(function(resolve, reject) {

            var userContactId = component.get("v.userContactId");
            if(userContactId) {
                var action = component.get('c.getContactInfo');
                action.setParams({
                    contactId : userContactId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        var response = response.getReturnValue();
                        var contact = JSON.parse(response)[0];
                        component.set("v.contact", contact);
                        self.hideCaseFields(component, event, helper);

                        resolve();
                    }
                    else {
                        reject($A.get('$Label.c.cs_get_contact_info_error'));
                    }
                });

                $A.enqueueAction(action);
            }
            else {
                resolve();
            }
        }));
    },

    getIPAddress : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var IPAction = component.get('c.getIPAddress');
            IPAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var response = response.getReturnValue();
                    component.set("v.IPAddress", JSON.parse(response).IP);
                    resolve();
                }
                else {
                   reject($A.get('$Label.c.cs_get_ip_error'));
                }
            });

            $A.enqueueAction(IPAction);
        }));
    },

    getAdditionalUserDetails : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var userInfoAction = component.get('c.getAdditonalUserDetails');
            userInfoAction.setParams({
                userId : $A.get("$SObjectType.CurrentUser.Id")
            });
            userInfoAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                }
            });

            //$A.enqueueAction(userInfoAction);
        }));
    },


    validateRequired : function(component, event, helper) {
        let namecomponent = component.find("name");
        let twitchUserNamecomponent = component.find("twitchUserName");

        if(component.get("v.name") != '') {
            namecomponent.set("v.errors", null);
        }

        if(component.get("v.twitchUserName") != '') {
            twitchUserNamecomponent.set("v.errors", null);
        }
    },

    categoryChange : function(component, event, helper) {
        var wrapper = component.get("v.webCaseWrapper");
        var loginAuthorized = component.get("v.loginAuthorized");

        var selCat = component.get("v.selectedCategory");
        var selSub = component.get("v.selectedSubCategory");
        var selDet = component.get("v.selectedDetailCategory");

        component.set("v.categoryArticle", null);
        component.set("v.showCategoryArticle", false);
        component.set("v.subCategoryArticle", null);
        component.set("v.showSubCategoryArticle", false);
        component.set("v.detailCategoryArticle", null);
        component.set("v.showDetailCategoryArticle", false);

        this.clearAll(component, event, helper);

        if(selCat) {
            let subCats = wrapper.catStrToSubCats[selCat];
            let subCategory;
            let cats = wrapper.categories;
            let category;
            for(let i = 0; i < cats.length; i++) {
                if(cats[i].Category__c == selCat) {
                    category = cats[i];
                    component.set("v.selectedCategoryWrapper", category);
                    break;
                }
            }

            if(category.Amazon_Login_Required__c) {
                this.loginToAmazon(component);
            }

            else if(category.Twitch_Login_Required__c) {
                if(loginAuthorized) {
                    var availableSubCats = wrapper.catStrToSubCats[selCat];
                    component.set("v.subCategories", availableSubCats);
                }
                else {
                    this.loginToPassport(component, event);
                }
            }

            else {
                var availableSubCats = wrapper.catStrToSubCats[selCat];
                if(availableSubCats) {
                    component.set("v.subCategories", availableSubCats);
                }

                else {
                    let blank = [];
                    component.set("v.subCategories", blank);
                }
            }

            var selectedArticle = wrapper.categoryToArticle[selCat];
            if(selectedArticle) {
                var body = selectedArticle.Body__c;
                if(body) {
                    component.set("v.categoryArticle", selectedArticle);
                    component.set("v.showCategoryArticle", true);
                }
            }

            else {
                component.set("v.categoryArticle", null);
                component.set("v.showCategoryArticle", false);
            }

            component.set("v.blockCaseCreate", category.Block_Case_Create__c);

        }
        else {
            this.clearAll(component, event);
            component.set("v.showSubjectAndMessage", false);
            component.set("v.blockCaseCreate", true);
        }

    },

    subCategoryChange : function(component, event, helper) {
        var selCat = component.get("v.selectedCategory");
        var selSub = component.get("v.selectedSubCategory");
        var wrapper = component.get("v.webCaseWrapper");
        var loginAuthorized = component.get("v.loginAuthorized");

        component.set("v.categoryArticle", null);
        component.set("v.showCategoryArticle", false);
        component.set("v.subCategoryArticle", null);
        component.set("v.showSubCategoryArticle", false);

        if(selSub != '') {
            var subCats = wrapper.catStrToSubCats[selCat];
            var subCategory;
            for(var i = 0; i < subCats.length; i++) {
                if(subCats[i].Sub_Category__c == selSub) {
                    subCategory = subCats[i];
                    component.set("v.selectedSubCategoryWrapper", subCategory);
                    break;
                }
            }

            component.set("v.showSubjectAndMessage", true);
            if(subCategory.Amazon_Login_Required__c) {
                this.clearDetail(component, event);
                this.loginToAmazon();
            }

            else if(subCategory.Twitch_Login_Required__c) {

                if(loginAuthorized) {
                    var availableDetCats = wrapper.subCatStrToDetCats[selSub];

                    if(availableDetCats) {
                        var spliceIndex = [];
                        for(var i = 0; i < availableDetCats.length; i++) {
                            if(availableDetCats[i].Category__c != selCat) {
                                spliceIndex.push(i);
                            }
                        }
                        for(var i = 0; i < spliceIndex.length; i++) {
                            availableDetCats.splice(availableDetCats[i], 1);
                        }


                        component.set("v.detailCategories", availableDetCats);
                        component.set("v.selectedDetailCategory", "");
                    }
                    else {
                        let blank = [];
                        component.set("v.detailCategories", blank);
                        component.set("v.selectedDetailCategory", "");
                    }

                }
                else {
                    this.clearAll(component, event);
                    this.loginToPassport(component, event);
                }
            }

                else {
                    var availableDetCats = wrapper.subCatStrToDetCats[selSub];

                    if(availableDetCats) {
                        var spliceIndex = [];
                        for(var i = 0; i < availableDetCats.length; i++) {
                            if(availableDetCats[i].Category__c != selCat) {
                                spliceIndex.push(i);
                            }
                        }
                        for(var i = 0; i < spliceIndex.length; i++) {
                            availableDetCats.splice(availableDetCats[i], 1);
                        }

                        component.set("v.detailCategories", availableDetCats);
                        component.set("v.selectedDetailCategory", "");
                    }
                    else {
                        let blank = [];
                        component.set("v.detailCategories", blank);
                        component.set("v.selectedDetailCategory", "");
                    }

                }


            var selectedArticle = wrapper.subCategoryToArticle[selSub];
            if(selectedArticle) {
                var body = selectedArticle.Body__c;
                if(body) {
                    component.set("v.subCategoryArticle", selectedArticle);
                    component.set("v.showSubCategoryArticle", true);
                }
            }

            else {
                component.set("v.subCategoryArticle", null);
                component.set("v.showSubCategoryArticle", false);
            }

            component.set("v.blockCaseCreate", subCategory.Block_Case_Create__c);

        }
        else {
            this.clearDetail(component, event);
            component.set("v.showSubjectAndMessage", false);
        }
    },

    detailCategoryChange : function(component, event, helper) {
        var selSub = component.get("v.selectedSubCategory");
        var selDet = component.get("v.selectedDetailCategory");
        var wrapper = component.get("v.webCaseWrapper");
        var loginAuthorized = component.get("v.loginAuthorized");
        component.set("v.categoryArticle", null);
        component.set("v.showCategoryArticle", false);
        component.set("v.subCategoryArticle", null);
        component.set("v.showSubCategoryArticle", false);
        component.set("v.detailCategoryArticle", null);
        component.set("v.showDetailCategoryArticle", false);

        if(selDet != '') {
            var detCats = wrapper.subCatStrToDetCats[selSub];
            var detailCategory;
            for(var i = 0; i < detCats.length; i++) {
                if(detCats[i].Detail_Category__c == selDet) {
                    detailCategory = detCats[i];
                    component.set("v.selectedDetailCategoryWrapper", detailCategory);
                    break;
                }
            }

            if(detailCategory.Amazon_Login_Required__c) {
               this.loginToAmazon();
            }
            else if(detailCategory.Twitch_Login_Required__c && (!loginAuthorized)) {
                this.loginToPassport(component, event);
            }

            var selectedArticle = wrapper.detailCategoryToArticle[selDet];
            if(selectedArticle) {
                var body = selectedArticle.Body__c;
                if(body) {
                    component.set("v.detailCategoryArticle", selectedArticle);
                    component.set("v.showDetailCategoryArticle", true);
                }
            }

            else {
                component.set("v.detailCategoryArticle", null);
                component.set("v.showDetailCategoryArticle", false);
            }

            component.set("v.showSubjectAndMessage", true);
            component.set("v.blockCaseCreate", detailCategory.Block_Case_Create__c);
        }
        else {
            component.set("v.showSubjectAndMessage", false);
        }
    },

    clearAll : function(component, event, helper) {
        var avail = [];
        component.set("v.subCategories", avail);
        component.set("v.selectedSubCategory", "");
        component.set("v.detailCategories", avail);
        component.set("v.selectedDetailCategory", "");
        component.set("v.showSubjectAndMessage", false);
        component.set("v.categoryArticle", '');
        component.set("v.subCategoryArticle", '');
        component.set("v.detailCategoryArticle", '');
        component.set("v.showCategoryArticle", false);
        component.set("v.showSubCategoryArticle", false);
        component.set("v.showDetailCategoryArticle", false);

    },

    clearDetail : function(component, event, helper) {
        var avail = [];
        component.set("v.detailCategories", avail);
        component.set("v.selectedDetailCategory", "");
        component.set("v.detailCategoryArticle", '');
        component.set("v.showDetailCategoryArticle", false);
        component.set("v.showSubjectAndMessage", false);
    },

    toggleReRender : function(component, event, helper) {
        var nameVal = component.get("v.name");
        var twitchUserNameVal = component.get("v.twitchUserName");
        var name = component.find("name");
        var twitchUserName = component.find("twitchUserName");

        if(nameVal) {
            $A.util.removeClass(twitchUserName, "slds-has-error");
            $A.util.addClass(twitchUserName, "hide-error-message");
        }
        if(twitchUserNameVal) {
            $A.util.removeClass(name, "slds-has-error");
            $A.util.addClass(name, "hide-error-message");
        }
    },

    toggleOptions : function(component, event, helper) {
        component.set("v.categories", component.get("v.categories"));
        component.set("v.subCategories", component.get("v.subCategories"));
        component.set("v.detailCategories", component.get("v.detailCategories"));
    },

    hideCaseFields : function(component, event, helper) {
        var currentContact = component.get("v.contact");
        var wrapper = component.get("v.webCaseWrapper");
        var categories = wrapper.categories;
        var subCategories = [];
        var detailCategories = [];

        for(var i = 0; i < categories.length; i++) {
            var category = categories[i].Category__c;
            var relatedCaseField = categories[i].Related_Case_Field__c;

            if(relatedCaseField) {
                if(currentContact[relatedCaseField] == false) {
                    categories[i].hide = true;
                }
                else {
                    categories[i].hide = false;
                }
            }

            var subCats = wrapper.catStrToSubCats[category];
            if(subCats) {
                for(var j = 0; j < subCats.length; j++) {
                    subCategories.push(subCats[j]);
                    var subCategory = subCats[j].Sub_Category__c;
                    var relatedCaseField = subCats[j].Related_Case_Field__c;

                    if(relatedCaseField) {
                        if(currentContact[relatedCaseField] == false) {
                            subCats[j].hide = true;
                        }
                        else {
                            subCats[j].hide = false;
                        }
                    }

                    var detCats = wrapper.subCatStrToDetCats[subCategory];
                    if(detCats) {
                        for(var k = 0; k < detCats.length; k++) {
                            detailCategories.push(detCats[k]);
                            var detailCategory = detCats[k].Detail_Category__c;
                            var relatedCaseField = detCats[k].Related_Case_Field__c;

                            if(relatedCaseField) {
                                if(currentContact[relatedCaseField] == false) {
                                    detCats[k].hide = true;
                                }
                                else {
                                    detCats[k].hide = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        component.set("v.webCaseWrapper", wrapper);
        component.set("v.categories", categories);
    },

    getURLParameters : function(component, event, helper) {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var selectedCategory = url.searchParams.get("selectedCategory");
        if(selectedCategory) {
            component.set("v.selectedCategory", selectedCategory);
            this.categoryChange(component, event);

        }
        var selectedSubCategory = url.searchParams.get("selectedSubCategory");
        if(selectedSubCategory) {
            component.set("v.selectedSubCategory", selectedSubCategory);
            this.subCategoryChange(component, event);

        }
        var selectedDetailCategory = url.searchParams.get("selectedDetailCategory");
        if(selectedDetailCategory) {
            component.set("v.selectedDetailCategory", selectedDetailCategory);
            this.detailCategoryChange(component, event);
        }

    },

    onRecordSubmit : function(component, event, helper) {
        var self = this;
        var fileList = component.get("v.fileList");
        var newCases = [];
        var newCase = {};
        newCase.sobjectType = 'Case';
        newCase.SuppliedName__c = component.get("v.name");
        newCase.TwitchUsername__c = component.get("v.twitchUserName");
        newCase.SuppliedEmail = component.get("v.email");
        newCase.Category__c = component.get("v.selectedCategory");
        newCase.SubCategory__c = component.get("v.selectedSubCategory");
        newCase.Detail_Category__c = component.get("v.selectedDetailCategory");
        newCase.ReadToSAgreement__c = component.get("v.ReadToSAgreement__c");
        newCase.ReadCommunityGuidelines__c = component.get("v.ReadCommunityGuidelines__c");
        newCase.IPAddress__c = component.get("v.IPAddress");
        newCase.TwitchUserSuspended__c = component.get("v.TwitchUserSuspended__c");
        newCase.TwitchOriginalAccountName__c = component.get("v.TwitchOriginalAccountName__c");
        newCase.TwitchOriginalSuspensionReason__c = component.get("v.TwitchOriginalSuspensionReason__c");
        newCase.TwitchAppealCaseNumber__c = component.get("v.TwitchAppealCaseNumber__c");
        newCase.SuspensionReason__c = component.get("v.SuspensionReason");
        newCase.Subject  = component.get("v.Subject ");
        newCase.Description  = component.get("v.Description ");
        newCase.OperatingSystem__c = component.get("v.OperatingSystem");
        newCase.Origin  = component.get("v.Origin ");
        newCase.BrowserInfo__c = component.get("v.BrowserInfo");
        newCases.push(newCase);
        var caseJSONString = JSON.stringify(newCases);

        var action = component.get('c.createCase');
        action.setParams({
            caseJSON : caseJSONString
        });
        action.setCallback(this, function(response) {
            var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " +
                             errors[0].message);
                }
            }
            var state = response.getState();
            if(state === "SUCCESS") {
                var caserecordId = response.getReturnValue();
                if(caserecordId != null) {
                    component.set("v.recordId", caserecordId);
                    if(fileList.length == 0){
                        self.onRecordSuccess(component, event, helper);
                    }else{
                        self.uploadHelper(component, event, helper);
                    }
                }
                let spinner = component.find("spinner");
                $A.util.addClass(spinner, 'slds-hide');
            }
            else {
                let spinner = component.find("spinner");
                $A.util.addClass(spinner, 'slds-hide');
                window.scrollTo(0,0);

                helper.showError(component, event, helper, $A.get('$Label.c.cs_case_submit_error'));
            }
        });

        $A.enqueueAction(action);
    },

    onRecordSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : component.get("v.successTitle"),
            message: ' ',
            type: 'Success',
            mode: 'dismissible'
        });
        toastEvent.fire();

        component.set("v.showForm", false);
    },

    loginToPassport : function(component, event, helper) {
        var action = component.get("c.auth_Passport");
        var selectedCategory = component.get("v.selectedCategory");
        var selectedSubCategory = component.get("v.selectedSubCategory");
        var selectedDetailCategory = component.get("v.selectedDetailCategory");
        action.setParams({
            selectedCategory : selectedCategory,
            selectedSubCategory : selectedSubCategory,
            selectedDetailCategory : selectedDetailCategory
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var authURL = response.getReturnValue();
                window.location.href=authURL;


            }
        });
        $A.enqueueAction(action);
    },

    redirectToPassport : function(component, event, helper) {
    	window.location.href = "/CS_RedirectToPassportLogin";
    },

    loginToAmazon : function(component) {
        var con = component.get("v.contact");
        var action = component.get("c.getAmazonRedirectInfo");
        action.setParams({
            countryCode : con.TwitchPrimeCountry__c
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            var amazonURL;
            if (state === "SUCCESS") {
                var amazonRedirects = JSON.parse(response.getReturnValue());
                for(var i = 0; i < amazonRedirects.length ; i++){
                    if(amazonRedirects[i].DeveloperName === con.TwitchPrimeCountry__c){
                        amazonURL = amazonRedirects[i].Help_URL__c;
                        break;
                    }else{
                        amazonURL = amazonRedirects[i].Help_URL__c;
                    }
                }
                window.location = amazonURL;
            }
        });
        $A.enqueueAction(action);
    },

    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb

    uploadHelper: function(component, event, helper) {
        var fileInput = component.get("v.fileList");
        // get the first file using array index[0]
        for (var i = 0; i < fileInput.length; i++) {
            this.readFile(component,fileInput[i]);
        }
    },

    readFile: function(component,file) {
        var objFileReader = new FileReader();
        var self = this;
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method
            self.uploadProcess(component, file, fileContents);
        });
        objFileReader.readAsDataURL(file);
    },

    uploadProcess: function(component, file, fileContents) {
        // set a default size or startpostiton as 0
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },


    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        // call the apex method 'saveChunk'
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveChunk");
        action.setParams({
            parentId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });

        // set call back
        action.setCallback(this, function(response) {
            // store the response / Attachment Id
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                // check if the start postion is still less then end postion
                // then call again 'uploadInChunk' method ,
                // else, diaply alert msg and hide the loading spinner
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } else {
                    //alert('your File is uploaded successfully');
                    //component.set("v.showLoadingSpinner", false);
                    this.onRecordSuccess(component, event);
                }
                // handel the response errors
            }
            else {
                window.scrollTo(0,0);
                helper.showError(component, event, helper, $A.get('$Label.c.cs_upload_file_error'));
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    },

    getBrowserInformation: function(component, event, helper) {
        navigator.sayswho= (function(){
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        })();

        component.set("v.browserInfo", navigator.sayswho);
    },

    setCaptchaListener: function(component, event, helper) {
        let vfOrigin = "https://twitcheng--dawnbackup--c.cs60.visual.force.com/apex/TWIT_Captcha";
        window.addEventListener("message", function(event) {
            if (event.data==="Unlock"){
                let myButton = component.find("myButton");
                myButton.set('v.disabled', false);
            }
        }, false);
    },

    handleUploadFinished: function(component, event, helper) {
        var uploadedFileList = event.getSource().get("v.files");
        var existingFileList = component.get("v.fileList");
        var fileList = [];

        for (var i = 0; i < existingFileList.length; i++) {
            fileList.push(existingFileList[i]);
        }
        for (var i = 0; i < uploadedFileList.length; i++) {
            fileList.push(uploadedFileList[i]);
        }
        component.set("v.fileList", fileList);
    },

    fireItemsChangeEvent: function(component, event, helper) {
        var appEvent = $A.get("e.selfService:caseCreateFieldChange");
        appEvent.setParams({
            "modifiedField": event.getSource().get("v.name"),
            "modifiedFieldValue": event.getSource().get("v.value")
        });
        appEvent.fire();
    },

    showError : function(component, event, helper, message) {
        var throwToastAction = $A.get('e.force:showToast');
        throwToastAction.setParams({
            'message': message,
            'type': 'error'
        });
        throwToastAction.fire();
    },

    getSuspensionReasonPicklist : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get("c.getSuspensionReasonOptions");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var returnValue = response.getReturnValue();
                    if (!returnValue.isSuccess) {
                        reject(returnValue.message);
                    }
                    else {
                        var options = [];
                        var optionMap = returnValue.options;
                        for (let i = 0; i < optionMap.length; i++) {
                            for (let key in optionMap[i]) {
                                options.push({ label: key, value: optionMap[i][key] });
                            }
                        }
                        component.set("v.SuspensionReasonOptions", options);
                        resolve();
                    }
                }
                else {
                    reject($A.get('$Label.c.cs_picklist_error'));
                }
            });
            $A.enqueueAction(action);
        }));
    }
    ,getOperatingSystemPicklist : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get("c.getOperatingSystemOptions");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var returnValue = response.getReturnValue();
                    if (!returnValue.isSuccess) {
                        reject(returnValue.message);
                    }
                    else {
                        var options = [];
                        var optionMap = returnValue.options;
                        for (let i = 0; i < optionMap.length; i++) {
                            for (let key in optionMap[i]) {
                                options.push({ label: key, value: optionMap[i][key] });
                            }
                        }
                        component.set("v.OperatingSystemOptions", options);
                        resolve();
                    }
                }
                else {
                    reject($A.get('$Label.c.cs_picklist_error'));
                }
            });
            $A.enqueueAction(action);
        }));
    },
})